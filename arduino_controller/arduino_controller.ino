#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "Wifi_Settings.h"


// Defining pins.
#define PIN_L_EN 5
#define PIN_R_EN 4
#define PIN_L_PWM 0
#define PIN_R_PWM 14

unsigned int udpBroadcastPort = 4210;
unsigned int tcpControlListener = 4211;
char incomingUDPPacket[255];

WiFiUDP Udp;
WiFiServer tcpControl(tcpControlListener);

// Doesn't allow the integer value to exceed the pwm values.
int limitValueToPwm(int value) {
  if(value > 1023)
    value = 1023;
  else if(value < 0)
    value = 0;
  return value;
}

// Set the pins to engage or disengage the motor.
void engageMotor(bool engage) {
  digitalWrite(PIN_L_EN, engage);
  digitalWrite(PIN_R_EN, engage);
}

// Sets the speed of the train.
void setSpeed(int speed) {
  Serial.printf("Speed set to: L:%i R:%i ::: %i\n", limitValueToPwm(speed), limitValueToPwm(speed * (-1)), speed);
  int lPWM = limitValueToPwm(speed);
  int rPWM = limitValueToPwm(speed* (-1));
  engageMotor(speed != 0);
  analogWrite(PIN_L_PWM, lPWM);
  analogWrite(PIN_R_PWM, rPWM);
}


void setup() {
  Serial.begin(9600);
  pinMode(PIN_L_EN, OUTPUT);
  pinMode(PIN_R_EN, OUTPUT);
  pinMode(PIN_L_PWM, OUTPUT);
  pinMode(PIN_R_PWM, OUTPUT);
  setSpeed(0);

  Serial.print("Connecting to WiFi");
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  // Wait for the wifi to connect.
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Wifi connected, IP Address: ");
  Serial.println(WiFi.localIP());
  Udp.begin(udpBroadcastPort);
  tcpControl.begin();

  Serial.println("Setup Method Complete.");
}


void loop() {
  // Check for a udp broadcast to find the device.
  int packetSize = Udp.parsePacket();
  if(packetSize) { // There is a packet to be read.
    int len = Udp.read(incomingUDPPacket, 255);
    if(len > 0) {
      incomingUDPPacket[len] = 0; // Put end character at the end of the string.
      // Reply to the packet to identify this device.
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write("CONTROLLER");
      Udp.endPacket();
    }
  }

  // Listening for direction commands from the train.
  WiFiClient tcpControlClient =  tcpControl.available();
  if(tcpControlClient) {
    Serial.println("[Client connected]");
    // There is a new client sending a request.
    while(tcpControlClient.connected()) {
      // Loop until the connection is closed.
      if(tcpControlClient.available()) {
        String output = tcpControlClient.readStringUntil('\r');
        Serial.println(output);
        if(output.equals("b\n")) {
          setSpeed(-1023);
        } else if(output.equals("f\n")) {
          setSpeed(1023);
        } else if(output.equals("s\n")) {
          setSpeed(0);
        }
      }
    }
    setSpeed(0);
    Serial.println("[Client disconnected]");
  }
}

