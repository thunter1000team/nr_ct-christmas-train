#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "Wifi_Settings.h"

#define PIN_SENSOR A0
#define SENSITIVITY 100

// Internet Settings.

unsigned int udpBroadcastPort = 4210;
unsigned int tcpCommandListenerPort = 4211;
char incomingUDPPacket[255];

int calibratedValue = 527;

IPAddress trainIp;

WiFiUDP Udp;

// Used for listening for stops which are sent from the client.
WiFiServer tcpCommandServer(tcpCommandListenerPort);
WiFiClient tcpCommandClient;
void startTrain(int i);
void setup() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  pinMode(PIN_SENSOR, INPUT);
  Serial.begin(9600);

  // Get the avrg background.
  int count = 0;
  calibratedValue = analogRead(PIN_SENSOR);
  for(int i = 0; i < 10; i++) {
    calibratedValue = (calibratedValue + analogRead(PIN_SENSOR)) / 2;
    delay(100);
  }
  Serial.printf("Calibrated: %i\n", calibratedValue);
  
  // Connect to wifi
  Serial.print("Connecting to WiFi");
  while(WiFi.status() != WL_CONNECTED) { // Wait until connected.
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Connected, IP Address: ");
  Serial.println(WiFi.localIP());

  Udp.begin(udpBroadcastPort);
  
  Serial.print("Finding Controller");
  // Use UDP broadcasts to find the controller.
  int packetSize = 0;
  do {
    Serial.print(".");
    Udp.beginPacket(WIFI_BROADCAST_ADDRESS, udpBroadcastPort);
    Udp.write("PING");
    Udp.endPacket();
    delay(500);
    packetSize = Udp.parsePacket();
    if(packetSize > 0) {
      trainIp = Udp.remoteIP();
    }
  } while(!packetSize);

  Serial.println();

  Serial.print("TRAIN IP: ");
  Serial.print(trainIp);
  // displayAddress(trainIp);
  Serial.println();

  tcpCommandServer.begin();
  tcpCommandClient = tcpCommandServer.available();
  tcpCommandClient.setNoDelay(true);
}

int outputtedState = 0;
int lastState = 0;
int count = 0;
int gotoStation = 0;
int direction = 0;



bool trainInMotion = false;
/**
 * Runs a TCP server to listen for train stops.
 */
void listenForCommand() {
  if(!trainInMotion) {
    if(tcpCommandClient && tcpCommandClient.connected()) {
      if(tcpCommandClient.available()) {
        Serial.println("Reading TCP Message");
        String line = tcpCommandClient.readStringUntil('\r');
        Serial.print("Message: ");
        Serial.print(line.toInt());
        Serial.println();
        // TODO process data.
        tcpCommandClient.println("OK");
        tcpCommandClient.stop();
        // tcpCommandServer.close();
        delay(10);
        startTrain(line.toInt());
      }
    } else {
      tcpCommandClient = tcpCommandServer.available();
    }
    delay(1);  
  }
}



WiFiClient tcpTrain;
void startTrain(int station) {
  if(station == count) {
    return;
  }
  Serial.print("Train started, going to station: ");
  Serial.println(station);
  tcpTrain.setNoDelay(true);
  if(!tcpTrain.connect(trainIp, 4211)) {
    Serial.println("Failed to connect to train");
    return;
  }

  trainInMotion = true;
  gotoStation = station;
  if(gotoStation > count) direction = 1; // Go to the station.
  else if(gotoStation < count) direction = -1;

  if(direction == 1) tcpTrain.print("f\n\r");
  else if(direction == -1) tcpTrain.print("b\n\r");

}

void stationCheck() {
  if(
    (gotoStation <= count && direction == 1) ||
    (gotoStation >= count && direction == -1) ||
    direction == 0) {
    // STOP THE TRAIN
    tcpTrain.stop();
    Serial.println("STOP");
    direction = 0;
    trainInMotion = false;
  }
}


long tcpKeepAliveCount = 0;
void loop() {
  int reading = 0;

  if(tcpKeepAliveCount > 2000 && tcpTrain.connected()){
    // tcpTrain.println("E");
    tcpKeepAliveCount = 0;
  }
  tcpKeepAliveCount++;

  if(outputtedState != 1 && analogRead(PIN_SENSOR) - calibratedValue > SENSITIVITY) {
    outputtedState = 1;
    Serial.println("S T");
  } else if(outputtedState != -1 && analogRead(PIN_SENSOR) - calibratedValue < -SENSITIVITY) {
    outputtedState = -1;
    Serial.println("N T");
  } else if(outputtedState != 0 && analogRead(PIN_SENSOR) - calibratedValue < SENSITIVITY && analogRead(PIN_SENSOR) - calibratedValue > -SENSITIVITY) {
    outputtedState = 0;
    // Serial.println("RESET");
  }

  if(lastState != outputtedState && outputtedState != 0) {  
    count += direction;
    Serial.printf("COUNT: %i\n", count);
    stationCheck();
  }

  if(outputtedState != 0)
    lastState = outputtedState;


  listenForCommand();
}
